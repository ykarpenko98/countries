//
//  WebViewViewController.swift
//  Countries
//
//  Created by Юрий on 2/22/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class WebViewViewController: UIViewController {
    
    var urlString: URL?
    
    @IBOutlet weak var wikiWebView: WKWebView!
    
    @IBAction func dismissWebViewController(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let request = URLRequest(url: urlString!)
        wikiWebView.load(request)
        
    }
}
