//
//  ViewController.swift
//  Countries
//
//  Created by Юрий on 2/20/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import UIKit

class CoutnriesViewController: UIViewController {
    
    private var countriesKeyArr = [String]() { 
        didSet {
            guard let tableView = tableView else { return }
            countriesKeyArr.sort { $0 < $1 } // sort by key
            /*countriesKeyArr.sort { CountryManager.shared().getCountry(country: $0)! < CountryManager.shared().getCountry(country: $1)! } // sort by name*/
            tableView.reloadData()
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkInternetConnectionAndSetTableData()
    }
    
    func checkInternetConnectionAndSetTableData() {
        if API.isConnectedToInternet() {
            setTableData()
        } else {
            
            let alert = UIAlertController(title: "No intertnet connection", message: "Tap to retry", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) in
                if !API.isConnectedToInternet() {
                    self.checkInternetConnectionAndSetTableData()
                    return
                }
                self.setTableData()
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    func setTableData() {
        CountryManager.shared().getCountryKeys { [weak self] (countriesKey) in
            DispatchQueue.main.async {
                if let c = countriesKey {
                    self?.countriesKeyArr = c
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
}

extension CoutnriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countriesKeyArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath) as! CountryTableViewCell
        let country = countriesKeyArr[indexPath.row]
        cell.countryKey = country
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetailCountry" {
            let controller = segue.destination as! DetailCounrtyViewController
            controller.countryKey = (sender as! CountryTableViewCell).countryKey
        }
    }
}
