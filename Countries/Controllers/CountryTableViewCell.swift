//
//  CountryTableViewCell.swift
//  Countries
//
//  Created by Юрий on 2/21/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var countryNameLabel: UILabel!
    
    var countryKey: String? = nil {
        didSet {
            setDataForCell()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.countryImageView.image = nil
    }
    
    func setDataForCell() {
        if let key = countryKey {
            countryNameLabel.text = CountryManager.shared().getCountry(country: key)
            setImageForCell(key)
        }
        
    }
    
    private var imageKey: String? = nil
    
    private func setImageForCell(_ key: String) {
        imageKey = key
        API.getImageForCountry(completion: { image in
            if key == self.imageKey {
                self.countryImageView.image = image
            } else {
                print("Ignore")
            }
        }, key: key)
    }
}
