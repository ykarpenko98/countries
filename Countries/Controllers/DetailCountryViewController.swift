//
//  DetailCountryViewController.swift
//  Countries
//
//  Created by Юрий on 2/22/19.
//  Copyright © 2019 Юрий. All rights reserved.
//


import UIKit

class DetailCounrtyViewController: UIViewController {
    
    var countryKey: String?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var flagImageView: UIImageView!
    
    @IBAction func wikiBarButtonItemDidPress(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Choose variation", message: "Please Select an Option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Open Safari", style: .default , handler:{ (UIAlertAction) in
            guard let url = self.getWikiUrlForCountry() else { return }
            UIApplication.shared.open(url)
        }))
        
        alert.addAction(UIAlertAction(title: "Open Web View", style: .default , handler: { (UIAlertAction) in
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebView") as? WebViewViewController {
                viewController.urlString = self.getWikiUrlForCountry()
                self.present(viewController, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.barButtonItem = sender
            popoverController.permittedArrowDirections = [.up]
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setInterface), name: NSNotification.Name(rawValue: "loadedDetailInformation"), object: nil)
        setInterface()
    }
    
    @objc func setInterface() {
        if let key = countryKey {
            DispatchQueue.main.async { [weak self] in
                if let strSelf = self {
                    strSelf.nameLabel.attributedText = strSelf.getTextForLabel()
                    API.getImageForCountry(completion: { image in
                        strSelf.flagImageView.image = image
                    }, key: key)
                }
            }
            
        }
    }
    
    func getTextForLabel() -> NSAttributedString {
        let cm = CountryManager.shared()
        
        let newString = NSMutableAttributedString()
        
        if let key = countryKey {
            let boldAttribute = [
                NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 18.0)!
            ]
            let regularAttribute = [
                NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Light", size: 18.0)!
            ]
            
            let nameBoldText = NSAttributedString(string: "Name:", attributes: boldAttribute)
            let nameRegularText = NSAttributedString(string: " \(cm.getCountry(country: key) ?? "N/A") \n\n", attributes: regularAttribute)
            newString.append(nameBoldText)
            newString.append(nameRegularText)
            
            let capitalBoldText = NSAttributedString(string: "Capital:", attributes: boldAttribute)
            let capitalRegularText = NSAttributedString(string: " \(cm.getCapital(country: key) ?? "N/A") \n\n", attributes: regularAttribute)
            newString.append(capitalBoldText)
            newString.append(capitalRegularText)
            
            let phoneCodeBoldText = NSAttributedString(string: "Phone Code:", attributes: boldAttribute)
            let phoneCodeRegularText = NSAttributedString(string: " \(cm.getPhoneCode(country: key) ?? "N/A") \n\n", attributes: regularAttribute)
            newString.append(phoneCodeBoldText)
            newString.append(phoneCodeRegularText)
            
            let currencyBoldText = NSAttributedString(string: "Currency:", attributes: boldAttribute)
            let currencyRegularText = NSAttributedString(string: " \(cm.getCurrency(country: key) ?? "N/A") \n\n", attributes: regularAttribute)
            
            newString.append(currencyBoldText)
            newString.append(currencyRegularText)
        }
        return newString
    }
    
    private func getWikiUrlForCountry() -> URL? {
        if let key  = self.countryKey,
            let name = CountryManager.shared().getCountry(country: key),
            let nameURL = "https://en.wikipedia.org/wiki/\(name)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            return URL(string: nameURL)
        }
        return nil
    }
    
    /*func checkInternetConnectionAndSetOtherData() {
     let alert = UIAlertController(title: "No intertnet connection", message: "Tap to retry", preferredStyle: .alert)
     alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { (UIAlertAction) in
     if !API.isConnectedToInternet() {
     return
     }
     self.setInterface()
     }))
     if !API.isConnectedToInternet() {
     self.present(alert, animated: true, completion: nil)
     } else {
     setInterface()
     }
     }*/
    
}
