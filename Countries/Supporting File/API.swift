//
//  API.swift
//  Countries
//
//  Created by Юрий on 2/20/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import Foundation
import Alamofire

class API {
    
    static func getCountries(completion: @escaping (DataResponse<Any>) -> Void) {
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)

        AF.request("http://country.io/names.json").responseJSON(queue: queue) {
            response in
            completion(response)
        }
    }
    
    static func getCapitalForCountry(completion: @escaping (DataResponse<Any>) -> Void) {
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        
        AF.request("http://country.io/capital.json").responseJSON(queue: queue) {
            response in
            completion(response)
        }
    }
    
    static func getPhoneCodeForCountry(completion: @escaping (DataResponse<Any>) -> Void) {
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        
        AF.request("http://country.io/phone.json").responseJSON(queue: queue) {
            response in
            completion(response)
        }
    }
    
    static func getCurrencyForCountry(completion: @escaping (DataResponse<Any>) -> Void) {
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        
        AF.request("http://country.io/currency.json").responseJSON(queue: queue) {
            response in
            completion(response)
        }
    }
    
    static func getImageForCountry(completion: @escaping (UIImage?) -> Void, key: String) {
        AF.request("https://www.countryflags.io/\(key)/flat/64.png").response { response in
            if let result = response.data {
                completion(UIImage(data: result))
            }
            else {
                completion(nil)
            }
        }
        
    }
    
    static func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
}

