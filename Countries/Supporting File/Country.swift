//
//  Country.swift
//  Countries
//
//  Created by Юрий on 2/21/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import Foundation

class Country {
    var key: String?
    var name: String?
    var capital: String?
    var phoneCode: String?
    var currencyCode: String?
    
    init(key: String, name: String) {
        self.key = key
        self.name = name
    }
    
}
