//
//  CountryManager.swift
//  Countries
//
//  Created by Юрий on 2/26/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import UIKit

class CountryManager: NSObject {
    
    private static let instance = CountryManager()
    
    public static func shared() -> CountryManager {
        return CountryManager.instance
    }
    
    //Country Name and Key Manager ----------------
    
    private var countriesD: [String: String]? = nil
    
    private var countriesKeyArr = [String]()
    

    public func getCountryKeys(completion: @escaping ([String]?) -> Void) {
        API.getCountries { (response) in
            if let result = response.result.value as? [String: String] {
                self.countriesD = result
                for (key,_) in result {
                    self.countriesKeyArr.append(key)
                }
                completion(self.countriesKeyArr)
            } else {
                completion(nil)
            }
        }
    }
    
    public func getCountry(country key: String) -> String? {
        return countriesD?[key]
    }
    
    //Capitals Manager -----------------
    
    private var capitals: [String: String]? = nil {
        didSet {
            postNotification()
        }
    }
    
    public func getCapital(country key: String) -> String? {
        return capitals?[key]
    }
    
    private func loadCapitalsForCountry() {
        API.getCapitalForCountry { respose in
            if let result = respose.result.value as? [String: String] {
                self.capitals = result
            }
            
        }
    }
    
    //Phone Code Manager ------------
    
    private var phoneCode: [String: String]? = nil {
        didSet {
            postNotification()
        }
    }
    
    public func getPhoneCode(country key: String) -> String? {
        return phoneCode?[key]
    }
    
    private func loadPhoneCodeForCountry() {
        API.getPhoneCodeForCountry { respose in
            if let result = respose.result.value as? [String: String] {
                self.phoneCode = result
            }
            
        }
    }
    
    //Currency Code ----------
    
    private var currency: [String: String]? = nil {
        didSet {
            postNotification()
        }
    }
    
    public func getCurrency(country key: String) -> String? {
        return currency?[key]
    }
    
    private func loadCurrencyForCountry() {
        API.getCurrencyForCountry { respose in
            if let result = respose.result.value as? [String: String] {
                self.currency = result
            }
            
        }
    }
    
    public func loadAllOtherDataForCountry() {
        DispatchQueue.global(qos: .background).async {
            sleep(5)
            self.loadCapitalsForCountry()
            sleep(5)
            self.loadPhoneCodeForCountry()
            sleep(5)
            self.loadCurrencyForCountry()
        }
    }
    
    private func postNotification() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadedDetailInformation"), object: nil)
    }
}
